<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Sign Up </title>
</head>
<body>
    <h1> Buat Account Baru! </h1>
    <h3> Sign Up Form </h3>
    <form action="/selamat" method="POST">
        @csrf
        <label for="fname"> First name: </label>
        <input type="text" name="first_name" id="fname">
        <br> <br>

        <label for="lname"> Last name: </label>
        <input type="text" name="last_name" id="lname">
        <br> <br>

        <label> Gender: </label> <br>
        <input type="radio" name="gender" value="0"> Male <br>
        <input type="radio" name="gender" value="1"> Female <br>
        <input type="radio" name="gender" value="2"> Other
        <br> <br>

        <label> Nationally: </label>
        <br> <br>
        <select name="" id="">
            <option value="indonesian"> Indonesian </option>
            <option value="singaporean"> Singporean </option>
            <option value="malaysian"> Malaysian </option>
            <option value="australian"> Australian </option>
        </select>
        <br> <br>

        <label> Language Spoken: </label>
        <br> <br>
        <input type="checkbox" namevalue="language_spoken" value="0"> Bahasa Indonesia <br>
        <input type="checkbox" namevalue="language_spoken" value="1"> English <br>
        <input type="checkbox" namevalue="language_spoken" value="2"> Other 
        <br> <br>

        <label for="biodata"> Bio: </label> 
        <br> <br>
        <textarea cols="35" rows="7" id="biodata"></textarea> <br>
        <button type="submit"> Sign Up </button>
        <br>
        <br>
    
    </form>
</body>
</html>