@extends(layouts.master)

@section
<div class="card-body">
                    <table class="table table-bordered">
                    <thead>                  
                        <tr>
                        <th style="width: 10px">#</th>
                        <th>Task</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>1.</td>
                        <td>Pertanyaan Pertama</td>
                        </tr>
                        <tr>
                        <td>2.</td>
                        <td>Pertanyaan Kedua</td>
                        </tr>
                        <tr>
                        <td>3.</td>
                        <td>Pertanyaan Ketiga</td>
                        </tr>
                        <tr>
                        <td>4.</td>
                        <td>Pertanyaan Keempat</td>
                        </tr>
                        <tr>
                        <td>5.</td>
                        <td>Pertanyaan Kelima/td>
                        </tr>
                        <tr>
                        <td>6.</td>
                        <td>Pertanyaan Keenam</td>
                        </tr>
                        <tr>
                        <td>7.</td>
                        <td>Pertanyaan Ketujuh</td>
                        </tr>
                        <tr>
                        <td>8.</td>
                        <td>Pertanyaan Kedelapan</td>
                        </tr>
                    </tbody>
                    </table>
    </div>
@endsection