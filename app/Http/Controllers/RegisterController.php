<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    
    public function register(){
        // dd($request->all());
        return view('form');
    }

    public function selamat(Request $request) {
        $fname = $request["first_name"];
        $lname = $request["last_name"];
        return view('selamat', compact("fname","lname"));
    }
}
